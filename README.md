# Myanmar Burmese ဗမာ

Digisec & digital safety resources in Burmese and other Myanmar languages

* [Digital First Aid Kit in Burmese](https://digitalfirstaid.org/my/)

* [A list of Guides/Tips/Readings which already localised in Burmese](https://bubbles.sevensnails.com/Guides_Tips_Readings/) - this is a living document

* [Digital Tea House](https://digitalteahouse.info/)  
a campaign launched by MIDO to promote digital safety awareness in Myanmar.

* [Risk mitigation and management guide](https://drive.google.com/file/d/1sTuOjE-HUCHEQlcNHMdWicbD0yu9Nzbb/view)  
Risk mitigation guide on communication strategies for frontline journalists and others in Myanmar in case of internet disruption

* [Myanmar Protesters Toolkit](http://freeexpressionmyanmar.org/wp-content/uploads/2021/02/myanmar-protesters-toolkit.pdf)  by Free Expression Myanmar

* [ဆန္ဒပြပွဲကို သွားမလား? သင့်ကိုယ်သင် ကာကွယ်ပါ!](https://protestos.org/my.html)  
Guide for going to a protest from [protestos.org](https://protestos.org), translated to Burmese language

* [Video as evidence - mini guides in Burmese](https://bubbles.sevensnails.com/Guides_Tips_Readings/VIDEO%20AS%20EVIDENCE-MINI%20GUIDES%20in%20Burmese.pdf) by Witness

* [**How to Bypass ‘Digital Dictatorship’ During the Myanmar Coup**
(originally published in Vice.com)](https://www.vice.com/en/article/dy8ekx/how-to-bypass-digital-dictatorship-during-the-myanmar-coup)  
Cryptpad backup in English (anonymous): https://cryptpad.fr/file/#/2/file/dwVf1LylTkeKvlfnj47a8pbw/  
Cryptpad backup in Myanmar (anonymous) https://cryptpad.fr/file/#/2/file/g2NtKtqlYSZ5tEBd6iaV69Ne/
